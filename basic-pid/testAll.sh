mkdir outputs 2>/dev/null

for i in {1..10}; do
    ./pan -a -N p$i -m1000000 > outputs/output-p$i.txt
    grep -i -B 1 "assertion violated" outputs/output-p$i.txt
    grep -i -B 1 "error:" outputs/output-p$i.txt
done
