/* single i/o processor psuedocode + application processor to send data */

/* data -> sensor information */
/* cmds -> actuator commands */

/* c = controller, p = peripherals */
mtype:TIOP_STATE = {tiop_init, recv_data, send_data, recv_cmds,
        send_cmds, monitor, rcf, error};

/* Booleans for different tests */
/* Test the application is running, if not wd should trigger */
bool AP_RUNNING;
/* Test the commands comming from the AP are valid */
bool CMDS_SAFE;
/* Test the data coming from the sensors is OK */
bool DATA_SAFE;


/* LTL Statements */
/* Safety */
/* Test tiop never enters error state */
ltl p1  { [](!tiop@STATE_ERROR) }

/* Test if all good, nothing bad happens */
ltl p2 { []((AP_RUNNING && CMDS_SAFE && DATA_SAFE) ->
        [](!tiop@RCF_ACTIVATED)) }


/* Test all bad settings cause errors */
ltl p3 { []((!AP_RUNNING) -> <>(tiop@RCF_ACTIVATED)) }

ltl p4 { []((!CMDS_SAFE) -> <>(tiop@RCF_ACTIVATED)) }

ltl p5 { []((!DATA_SAFE) -> <>(tiop@RCF_ACTIVATED)) }

/* Liveness */
/* Test that if sensor data is available, actuator commands will be sent */
ltl p6 { []((tiop@DATA_RECEIVED) -> <>(tiop@COMMANDS_SENT)) }

#define MAX_DATA 2016
#define MIN_DATA -2048
#define MAX_CMDS 2016
#define MIN_CMDS -2048
#define SAFETY_VAL 0

#define NUM_VARS 2

chan uart_data = [NUM_VARS] of {short};
chan uart_cmds = [NUM_VARS] of {short};
chan fifo_data = [NUM_VARS] of {short};
chan fifo_cmds = [NUM_VARS] of {short};

active proctype tiop() {
    mtype:TIOP_STATE state = tiop_init;
    mtype:TIOP_STATE prev_state;
    /* tx byte, rx byte and counter for timer */
    short data[NUM_VARS], cmds[NUM_VARS];
    bool data_error, cmds_error, wd_triggered;
    int i;
    do
    :: (state == tiop_init) ->
        prev_state = tiop_init;
        state = recv_data;
        data_error = false;
        cmds_error = false;
        wd_triggered = false;
    :: (state == recv_data) ->
        for (i : 0 .. (NUM_VARS - 1)) {
            uart_data?data[i];
        }
DATA_RECEIVED:
        prev_state = recv_data;
        state = monitor;
    :: (state == send_data) ->
        for (i : 0 .. (NUM_VARS - 1)) {
            fifo_data!data[i];
        }
        prev_state = send_data;
        state = recv_cmds;
    :: (state == recv_cmds)  ->
        prev_state = state;
        for (i : 0 .. (NUM_VARS - 1)) {
            if 
            :: fifo_cmds?cmds[i] ->
                wd_triggered = false;
                state = monitor;
            :: timeout ->
                wd_triggered = true;
                state = rcf;
                break;
            fi
        }
    :: (state == send_cmds) ->    
        for (i : 0 .. (NUM_VARS - 1)) {
            uart_cmds!cmds[i];
        }
COMMANDS_SENT:
        prev_state = send_cmds;
        state = recv_data;
    :: (state == monitor) ->
        if
        :: (prev_state == recv_data) ->
            state = send_data;
            for (i : 0 .. (NUM_VARS - 1)) {
                if
                :: (data[i] > MAX_DATA || data[i] < MIN_DATA) ->
                    data_error = true;
                    break;
                :: else ->
                    data_error = data_error | false;
                fi
            }
        :: (prev_state == recv_cmds) ->
            for (i : 0 .. (NUM_VARS - 1)) {
                if
                :: (cmds[i] > MAX_CMDS || cmds[i] < MIN_CMDS) ->
                    cmds_error = true;
                    break;
                :: else ->
                    cmds_error = false;
                fi
            }

            if
            :: (wd_triggered || data_error || cmds_error) ->
                state = rcf;
            :: else ->
                state = send_cmds;
            fi
        :: else ->
            state = error;
        fi
        prev_state = monitor;
    :: (state == rcf) ->
RCF_ACTIVATED:
        for (i : 0 .. (NUM_VARS - 1)) {
            cmds[i] = SAFETY_VAL;
        }
        prev_state = rcf;
        state = send_cmds;
    :: (state == error) ->
STATE_ERROR:
        prev_state = state;
        break;
    od;
}


mtype:AP_STATE = {ap_init, ap_recv, ap_pid, ap_send, ap_error};

active proctype ap() {
    short data[NUM_VARS], cmds[NUM_VARS];
    int i;
	mtype:AP_STATE ap_state = ap_init;
    do
    :: (ap_state == ap_init) ->
        ap_state = ap_recv;
    :: (ap_state == ap_recv) ->
        for (i : 0 .. (NUM_VARS - 1)) {
            fifo_data?data[i];
        }
        ap_state = ap_pid;
    :: (ap_state == ap_pid) ->
        for (i : 0 .. (NUM_VARS - 1)) {
            if
            :: (CMDS_SAFE) ->
                    cmds[i] = data[i];
            :: else ->
                if
                :: cmds[i] = MAX_CMDS + 1;
                :: cmds[i] = MIN_CMDS - 1;
                fi
            fi
        }
        if
        :: (AP_RUNNING) ->
            ap_state = ap_send;
        :: else ->
            ap_state = ap_recv;
        fi
    :: (ap_state == ap_send) ->
        for (i : 0 .. (NUM_VARS - 1)) {
            fifo_cmds!cmds[i];
        }
        ap_state = ap_recv;
    :: (ap_state == ap_error) ->
        break;
    od
}

active proctype sensors() {
    short data[NUM_VARS];
    int i;
    do
    :: true ->
        for (i : 0 .. (NUM_VARS - 1)) {
            if
            :: (DATA_SAFE) ->
                data[i] = SAFETY_VAL;
            :: else ->
                if
                :: data[i] = MAX_DATA + 1;
                :: data[i] = MIN_DATA - 1;
                fi
            fi
            uart_data!data[i];
        }
    od
}

active proctype actuators() {
    short cmds[NUM_VARS];
    int i;
    do
    :: (true) ->
        for (i : 0 .. (NUM_VARS - 1)) {
            uart_cmds?cmds[i];
        }
    od
}
