mkdir outputs 2>/dev/null

passed=0
failed=0
for i in {1..6}; do
    ./pan -a -n -N p$i -m100000 > outputs/output-p$i.txt
    ( grep -iq -B 1 "assertion violated" outputs/output-p$i.txt || \
    grep -iq -B 1 "error:" outputs/output-p$i.txt || \
    grep -iq -B 1 "errors: [1-9][0-9]*" outputs/output-p$i.txt ) && \
    { ((failed++)); echo p$i Failed; } || { ((passed++)); echo p$i Passed; }
done

echo $passed tests passed, $failed failed.
