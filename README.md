# spin
This repo is used to track various model checking programs created using spin, used to verify i/o processors in my thesis.

To build run: 
```
make spin
make build
```
OR
```
make all
```
To test a ltl run:
```
./pan -a -N p[1-10] -m1000000
```
